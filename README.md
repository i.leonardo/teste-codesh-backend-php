# Teste Codesh - Back-end PHP

> Use **PHP 7.4.8**

Crud simples para o cadastro de produtos, usando como base o arquivo JSON `products.json`

![main1](https://gitlab.com/i.leonardo/teste-codesh-backend-php/-/raw/master/main1.png)

![main2](https://gitlab.com/i.leonardo/teste-codesh-backend-php/-/raw/master/main2.png)

![main3](https://gitlab.com/i.leonardo/teste-codesh-backend-php/-/raw/master/main3.png)

## Requisitos

> - [Lumen](https://lumen.laravel.com/): **v7*** - Micro Framework Laravel
> - **Dependências:**
>   - Sem dependências adicionais
> - Obrigatório editar `.env`:
>
>   - **DB_CONNECTION** - Tipo de conexão do banco de dados
> - Use **Insomnia Designer** para visualizar os endpoints do `openapi.yml`
> - **Editor usado:**
> - Visual Studio Code
>     - [editorconfig.editorconfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig): **EditorConfig for VS Code**
>     - [kokororin.vscode-phpfmt](https://marketplace.visualstudio.com/items?itemName=kokororin.vscode-phpfmt): **PHP Formatter**

## Project Setup

```bash
# install dependencies
composer install

# Update Class
composer dump-autoload

# DB
php artisan migrate --seed

# Compiles and hot-reloads for development (port 8000)
composer run serve
```

## Árvore (tree)

```
.
|   .editorconfig
|   .env (Config)
|   .gitignore
|   artisan (Artisan CLI)
|   composer.json
|   openapi.yml (View API)
|   phpunit.xml
|   README.md
|
+---app
|   +---Http
|   |   \---Controllers
|   |       \   ProductController.php
|   |
|   \---Models
|       \   Product.php
|
+---bootstrap
+---database
|   +---migrations
|   \---seeds
|
\---routes
```

## Licença

> MIT License

Copyright (c) 2020 - Leonardo C. Carvalho
