<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'title',
        'type',
        'description',
        'filename',
        'price',
        'width',
        'height',
        'rating',
    ];
    protected $hidden = ['created_at', 'updated_at'];
}
