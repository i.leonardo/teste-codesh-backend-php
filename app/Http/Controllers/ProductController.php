<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $product_rules = [
        'title' => 'required|max:255',
        'type' => 'required|max:255',
        'description' => 'required',
        'filename' => 'required',
        'price' => 'required|between:0,99.99',
        'width' => 'required|numeric',
        'height' => 'required|numeric',
        'rating' => 'required|numeric',
    ];

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function index()
    {
        return response()->json($this->product::all());
    }

    public function show($id)
    {
        return response()->json($this->product->find($id));
    }

    public function store(Request $request)
    {
        //validation
        $this->validate($request, $this->product_rules);

        // insert record
        $data = $this->product->create($request->all());
        return response()->json($data, 201);
    }

    public function update($id, Request $request)
    {
        // validation
        $this->validate($request, $this->product_rules);

        // update record
        $data = $this->product->findOrFail($id);
        $data->update($request->all());
        return response()->json($data, 200);
    }

    public function delete($id)
    {
        $this->product->findOrFail($id)->delete();
        return response('Deleted successfully', 200);
    }
}
