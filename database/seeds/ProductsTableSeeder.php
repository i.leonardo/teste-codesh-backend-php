<?php

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = \DB::table('products');

        if ($product->get()->count()) {
            $product->delete();
        }
        $this->create();
    }

    /**
     * Create Database and insert Data
     *
     * @return void
     */
    private function create()
    {
        $path = realpath('documentation/products.json');
        $data = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', file_get_contents($path)));

        foreach ($data as $obj) {
            Product::create([
                'title' => $obj->title,
                'type' => $obj->type,
                'description' => $obj->description,
                'filename' => $obj->filename,
                'price' => $obj->price,
                'width' => $obj->width,
                'height' => $obj->height,
                'rating' => $obj->rating,
            ]);
        }
    }

}
